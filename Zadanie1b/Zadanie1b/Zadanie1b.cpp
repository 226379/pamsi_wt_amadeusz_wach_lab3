// Zadanie2.cpp
// Amadeusz Wach

#include "stdafx.h"
#include <iostream>
#include <stack>

using namespace std;
int main()
{
	stack<int> S;

	bool czyKoniec = false;
	int wybor;


	string word;
	while (!czyKoniec) {
		cout << "1. Dodaj element" << endl;
		cout << "2. Usun element" << endl;
		cout << "3. Usun wszystkie elementy" << endl;
		cout << "4. Wyswietl elementy stosu" << endl;
		cout << "5. Koniec programu" << endl;
		cout << "Twoj wybor: ";
		cin >> wybor;
		system("cls");
		switch (wybor) {
		case 1: {
			int wartosc;
			cout << "Podaj wartosc: ";
			cin >> wartosc;
			S.push(wartosc);
			break;
		}
		case 2: {
			S.pop();
			break;
		}

		case 3: {
			while(!S.empty())
				S.pop();
			break;
		}
		case 4: {
			stack<int> tmp = S;
			while (!tmp.empty()) {
				cout << tmp.top() << "\n";
				tmp.pop();
			}
			break;
		}
		case 5: {
			czyKoniec = true;
			break;
		}

		default: {
			cout << "Nie znaleziono opcji, sprobuj ponownie!" << endl;
		}
		}
	}

	getchar();
	return 0;
}

