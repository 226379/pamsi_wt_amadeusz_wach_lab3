#pragma once

template <typename T> class Stos {
	T * elementy;
	int rozmiar;
	int wierzch;
	void powieksz();
public:
	Stos() {
		rozmiar = 0;
		wierzch = -1;
	}
	Stos(int _rozmiar) {
		T*elementy = new T[_rozmiar];
		rozmiar = _rozmiar;
		wierzch = -1;
	}
	~Stos() {
		delete[] elementy;
	}

	bool czyPusta();
	void dodaj(T _wartosc);
	T usun();
	void usunWszystko();
	void wyswietl();
};

template <typename T>
void Stos<T>::powieksz() {
	T * tmp = new T[rozmiar+1];
	for (int i = 0; i < rozmiar; i++)
		tmp[i] = elementy[i];
	delete[] elementy;
	elementy = tmp;
	rozmiar++;
}

template <typename T>
bool Stos<T>::czyPusta() {
	if (wierzch == -1)
		return true;
	return false;
}

template <typename T>
void Stos<T>::dodaj(T _wartosc) {
	if (wierzch + 1 == rozmiar) 
		powieksz();
	wierzch++;
	elementy[wierzch] = _wartosc;
}

template <typename T>
T Stos<T>::usun() {
	if (!czyPusta()) {
		return  elementy[wierzch];
		wierzch--;
	}
}

template <typename T>
void Stos<T>::usunWszystko() {
	wierzch = -1;
}

template <typename T>
void Stos<T>::wyswietl() {
	for (int i = wierzch; i >= 0; i--) {
		std::cout << elementy[i] << "\n";
	}
}

