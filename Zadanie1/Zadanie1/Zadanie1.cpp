// Zadanie2.cpp
// Amadeusz Wach

#include "stdafx.h"
#include <iostream>
#include "Stos.h"

using namespace std;
int main()
{
	Stos<int> S;

	bool czyKoniec = false;
	int wybor;
	
	
	string word;
	while (!czyKoniec) {
		cout << "1. Dodaj element" << endl;
		cout << "2. Usun element" << endl;
		cout << "3. Usun wszystkie elementy" << endl;
		cout << "4. Wyswietl elementy stosu" << endl;
		cout << "5. Koniec programu" << endl;
		cout << "Twoj wybor: ";
		cin >> wybor;
		system("cls");
		switch (wybor) {
		case 1: {
			int wartosc;
			cout << "Podaj wartosc: ";
			cin >> wartosc;
			S.dodaj(wartosc);
			break;
		}
		case 2: {
			S.usun();
			break;
		}

		case 3: {
			S.usunWszystko();
			break;
		}
		case 4: {
			S.wyswietl();
			break;
		}
		case 5: {
			czyKoniec = true;
			break;
		}

		default: {
			cout << "Nie znaleziono opcji, sprobuj ponownie!" << endl;
		}
		}
	}

	getchar();
	return 0;
}

