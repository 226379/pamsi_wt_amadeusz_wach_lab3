// Zadanie4.cpp 
//

#include "stdafx.h"
#include <iostream>
#include <stack>
using namespace std;
void hanoi(int n, stack<int> * A, stack<int> * B, stack<int> * C)
{
	if (n > 0)
	{
		hanoi(n - 1, A, C, B);
		C->push(A->top());
		A->pop();
		hanoi(n - 1, B, A, C);
	}
}

int main()
{
	int n;
	cout << "Podaj ilosc krazkow: ";
	cin >> n;

	stack<int> * A = new stack<int>;
	stack<int> * B = new stack<int>;
	stack<int> * C = new stack<int>;

	for (int i = n; i > 0; i--) {
		A->push(i);
	}
	hanoi(n, A, B, C);
	for (int i = 1; i <= n; i++) {
		cout << C->top() << "\n";
		C->pop();
	}


	delete C;
	delete B;
	delete A;
	cin.ignore();
	getchar();
    return 0;
}

