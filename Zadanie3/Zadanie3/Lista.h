#pragma once

#include "Wezel.h"
using namespace std;
template <typename T> class Lista {
	Wezel<T> * glowa;
	Wezel<T> * ogon;
public:
	Lista() {
		glowa = nullptr;
		ogon = nullptr;
	}
	void dodajPrzod(T _wartosc);
	void dodajTyl(T _wartosc);
	void usunPrzod();
	//void usunTyl();
	void wyswietl();
	void usunWszystko();
	bool czyPusta();
};


template <typename T>
void Lista<T>::dodajPrzod(T _wartosc) {
	Wezel<T> * nowy = new Wezel<T>;
	nowy->setWartosc(_wartosc);
	if (czyPusta()) {
		glowa = nowy;
		ogon = nowy;
	}
	else {
		nowy->setNastepny(glowa);
		glowa = nowy;
	}
}

template <typename T>
void Lista<T>::dodajTyl(T _wartosc) {
	Wezel<T> * nowy = new Wezel<T>;
	nowy->setWartosc(_wartosc);
	if (czyPusta()) {
		glowa = nowy;
		ogon = nowy;
	}
	else {
		ogon->setNastepny(nowy);
		ogon = nowy;
	}
}

template <typename T>
void Lista<T>::usunPrzod() {
	Wezel<T> * tmp = glowa;
	if (!czyPusta()) {
		if (tmp == ogon) {
			delete tmp;
			ogon = nullptr;
			glowa = nullptr;
		}
		else {
			glowa = tmp->getNastepny();
			delete tmp;
		}
	}
}

/*template <typename T>
void Lista<T>::usunTyl() {
Wezel<T> * tmp = glowa;
if (!czyPusta()) {
Wezel<T> * tmp = glowa->getNastepny();
delete glowa;
glowa = tmp;
}
}
*/
template <typename T>
void Lista<T>::wyswietl() {
	Wezel<T> * tmp = glowa;
	if (!czyPusta()) {
		while (tmp) {
			cout << tmp->getWartosc() << endl;
			tmp = tmp->getNastepny();
		}
	}
	else cout << "Lista jest pusta!" << endl;
}

template <typename T>
void Lista<T>::usunWszystko() {
	while (!czyPusta())
		usunPrzod();
}

template <typename T>
bool Lista<T>::czyPusta() {
	if (glowa)
		return false;
	return true;
}
