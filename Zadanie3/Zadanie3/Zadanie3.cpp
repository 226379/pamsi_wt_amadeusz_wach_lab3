// Zadanie3.cpp
// Amadeusz Wach

#include "stdafx.h"
#include <iostream>
//#include <stack>
//#include "Stos.h"
#include "Lista.h"

#include <ctime>
#include <cstdlib>
#include <chrono>

using namespace std;

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<float> fsec;

int main()
{
	
	
	srand(time(0));
	fsec czas;

	int n[] = { 1000, 10000, 100000, 500000 };
	int ile_pomiarow = 4;
	for (int j = 0; j < ile_pomiarow; j++) {
		//Stos<int> S;
		//stack<int> S;
		Lista<int> S;
		auto start = Time::now();
		for (int i = 0; i < n[j]; i++) {
			//S.dodaj(rand() % 10);
			//S.push(rand() % 10);
			S.dodajPrzod(rand() % 10);
		}
		auto stop = Time::now();
		czas = stop - start;
		cout << "Dla " << n[j] << " czyli " <</* S.getRozmiar() * S.size()*/ " danych czas wykonania: " << czas.count() << "\n";
	}
	
	getchar();
	return 0;
}

